package model

type gopher struct {
	name    string
	age     int
	isAdult bool
}

func (g gopher) Jump() string {
	if g.age < 65 {
		return g.name + " can jump HIGH"
	}
	return g.name + " can still jump"
}

type horse struct {
	name   string
	weight int
}

func (h horse) Jump() string {
	if h.weight > 2500 {
		return "I'm too heavy, can't jump"
	}
	return "I will jump, neigh!!!"
}

type jumper interface {
	Jump() string
}

func GetList() []jumper {
	edu := &gopher{name: "Edu", age: 49}
	alex := &gopher{name: "Alex", age: 70}
	barbaro := &horse{name: "Barbaro", weight: 2000}

	list := []jumper{edu, alex, barbaro}
	return list
}

func validateAge(g *gopher) {
	g.isAdult = g.age > 18
}
