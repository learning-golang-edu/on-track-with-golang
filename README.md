# "hello" Golang application

This is simple *hello* golang application.

## Setup environment

To build and run *go* application and to use *go* tools need to set *GOPATH* and add it to *PATH*. Run following command in the project root directory (directory were *src* sub-directory is located):

    $ export GOPATH=$(pwd)
    $ export PATH=$GOPATH/bin:$PATH

This will set current directory as *GOPATH* and add *bin* sub-directory located in *GOPATH* directory to global *PATH*.

## Build

To build application run following command:

    $ cd src/hello/
    $ go build

This command will create executable in same directory. It can be run with the following command:

    $ ./hello
    Hello, I am Gopher

## Build and run

Application can be run right away without a need to build it separately. To run it execute following command:

    $ go run main.go
    Hello, I am Gopher

## Running in parallel

### Preparing for parallel run

*main* function:
    func main() {
        names := []string{"Arno", "Lauri", "Alisa", "Karoliina"}
        for _, name := range names {
            printName(name)
        }
    }

*printName* function:

    func printName(n string) {
       fmt.Println("Name: ", n)
    }

To run program with *time* command to check execution time execute following command:

    $ time go run main.go
    ...
    real    0m0.318s

For simple program which prints a strings into standard output it takes less than half a second.

### Making program more time consuming

With time consuming math calculation to *printName* function:

    func printName(n string) {
        result := 0.0
        for i := 0; i < 100000000; i++ {
            result += math.Pi * math.Sin(float64(len(n)))
        }
        fmt.Println("Name: ", n)
    }

this time is increased to:

    real    0m6.904s

*go* keyword create new *goroutine* which is run in parallel with main process. Adding *go* call to the method is not enough, since *main* function doesn't know about newly create *goroutine* and will exit immediately.

    func main() {
        names := []string{"Arno", "Lauri", "Alisa", "Karoliina"}
        for _, name := range names {
            go printName(name)
        }
    }

This program will exit immediately and will not print any names:

    $ time go run main.go
    real    0m0.252s

To make *main* function aware of *goroutine* we can use *sync.WaitGroup*. We need to add number of *goroutine*s to be run to the *sync.WaitGroup* with *Add* function and after *go* call this new group should wait for each *goroutine* finish is run with *Wait* function. And reference to *sync.WaitGroup* variable should be passed to function which is run in parallel.

    func main() {
        names := []string{"Arno", "Lauri", "Alisa", "Karoliina"}
        var wg sync.WaitGroup
        wg.Add(len(names))
        for _, name := range names {
            go printName(name, &wg)
        }
        wg.Wait()
    }

Called function at the end of its execution should call *Done* function of *sync.WaitGroup* variable.

    func printName(n string, wg *sync.WaitGroup) {
        result := 0.0
        for i := 0; i < 100000000; i++ {
            result += math.Pi * math.Sin(float64(len(n)))
        }
        fmt.Println("Name: ", n)
        wg.Done()
    }

If we run this program with only 1 CPU used, it will take same time as program without *goroutines*. This will run each *goroutine* concurrently. You can see it from the changed order of printed names. But there would be no improvement in performance when run concurrently on 1 CPU.

    $ time GOMAXPROCS=1 go run main.go
    Name:  Karoliina
    Name:  Arno
    Name:  Lauri
    Name:  Alisa

    real    0m6.937s

Go runtime by default utilises all processors available. Our concurrent code will run in parallel on machine with multiple CPUs.

    $ time go run main.go
    Name:  Lauri
    Name:  Karoliina
    Name:  Alisa
    Name:  Arno

    real    0m1.999s

When concurrent code is run in parallel on multiple CPUs, we can see significate improvnemt in program performance.

## Code formatting

Golang have built-in tool to format code. To format it execute following command:

    $ gofmt -w main.go

Most editors and IDEs have plugins, which do the same on file save.

## Import fixing

Go have tool to fix imports *goimports*. On top of fixing go file imports it also format go code. But this tool does not come as part of standard go installation. It should be installed separately.

To install this tool execute following command:

    $ go get golang.org/x/tools/cmd/goimports

To run this tool execute followint command:

    $ cd src/hello
    $ goimports -w main.go

It will fix imports and format the code.