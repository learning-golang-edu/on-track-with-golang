# "hello" Golang application

This is simple *hello* golang application.

## Build

To build application run following command:

    $ cd src/hello/
    $ go build

This command will create executable in same directory. It can be run with the following command:

    $ ./hello 
    Hello, I am Gopher

## Build and run

Application can be run right away without a need to build it separately. To run it execute following command:

    $ go run main.go
    Hello, I am Gopher

## Code formatting

Golang have built-in tool to format code. To format it execute following command:

    $ gofmt -w main.go

Most editors and IDEs have plugins, which do the same on file save.