# "hello-cli" Golang application

This is simple *hello* golang application.

## Build

To build application run following command:

    $ cd src/hello/
    $ go build

This command will create executable in same directory. It can be run with the following command:

    $ ./hello 
    Hello, I am Gopher

## Build and run

Application can be run right away without a need to build it separately. To run it execute following command:

    $ go run main.go
    Hello, I am Gopher

## Code formatting

Golang have built-in tool to format code. To format it execute following command:

    $ gofmt -w main.go

Most editors and IDEs have plugins, which do the same on file save.

## Import fixing

Go have tool to fix imports *goimports*. On top of fixing go file imports it also format go code. But this tool does not come as part of standard go installation. It should be installed separately.

To install this tool execute following command:

    $ go get golang.org/x/tools/cmd/goimports

To run this tool execute followint command:

    $ cd src/hello-cli
    $ goimports -w main.go

It will fix imports and format the code.